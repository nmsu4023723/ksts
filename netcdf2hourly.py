# Convert hourly ERA5 netCDF data files from Copernicus to files
# consumed by KSTS simulator.  Converts raw variables to power values.

import sys
# use `NetCDFFile` from the `scipy.io` namespace
from scipy.io import netcdf_file
import numpy as np
import datetime, calendar

infiles = sys.argv[1:]

WIND_POWER_COEFFICIENTS = (1.33661914e-01,
                           -6.17273350e+00,
                           1.04731925e+02,
                           -8.02951211e+02,
                           2.95758034e+03,
                           -4.17083376e+03)
def my_polyval(m, x):
    y = np.zeros(x.shape)
    for i,j in enumerate(range(len(m) - 1, -1, -1)):
        y += m[j] * x**i
    y[x < 3.7] = 0.
    y[x > 12.9] = 2000.
    y[x > 25] = 0.
    return y


def days_in_hour_range(hour1, hour2):
    basedate = datetime.date(1900, 1, 1)
    
    time1 = basedate + datetime.timedelta(hours=hour1)
    time2 = basedate + datetime.timedelta(hours=hour2)
    delta = time2 - time1
    return delta.total_seconds() / 60 / 60 / 24


def get_files_range(files):
    first_hour = int(1e10)
    last_hour = 0
    hours = 0
    lonstart = None
    yeardrop = 0   # hack (disabled)
    for nfile in files:
        with netcdf_file(nfile,'r') as incdf:
            time = incdf.variables['time']
            hours += time.shape[0]
            first_hour = min(first_hour, time[0])
            last_hour = max(last_hour, time[-1])

            # longitude = incdf.variables['longitude']
            # latitude = incdf.variables['latitude']
            # if lonstart is None:
            #     lonstart = longitude[0]
            #     latstart = latitude[0]
            #     lonlen = longitude.shape[0]
            #     latlen = latitude.shape[0]

            first_pytime = datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(time[0]))
            last_pytime =  datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(time[-1]))
            first_year = first_pytime.year
            last_year = last_pytime.year
            print(first_year, last_year, time.shape[0])
            # savetime = time[:]
            time = longitude = latitude = None
    first_pytime = datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(first_hour))
    last_pytime =  datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(last_hour))
    first_year = first_pytime.year
    last_year = last_pytime.year
    if (last_hour - first_hour + 1) != hours:
        print('Times not as expected')
        print(hours/8760.)
        print("failed hour count check",hours - (last_hour - first_hour + 1))
        exit()
        # year 2000 appears in two files
        for i in range(0, len(savetime), 1):
            t = datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(savetime[i]))
            print(t)
        sys.exit(1)
    return first_hour, last_hour

# hours since 1900-01-01 00:00:00.0
incdf = netcdf_file(infiles[0], 'r')
time = incdf.variables['time']
first_pytime = datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(time[0]))
last_pytime =  datetime.date(1900, 1, 1) + datetime.timedelta(hours=int(time[-1]))
time = None
longitude = incdf.variables['longitude']
latitude = incdf.variables['latitude']
lonlen = longitude.shape[0]
latlen = latitude.shape[0]
longitude = latitude = None
incdf.close()
ngrid = lonlen * latlen
assert ngrid == 575  # New Mexico download

print("Time range of first input file: %s - %s" % (first_pytime, last_pytime))
assert first_pytime.month == 1
assert first_pytime.day == 1
assert last_pytime.month == 12
assert last_pytime.day == 31
first_year = first_pytime.year
last_year = last_pytime.year

files = sys.argv[1:]
starth, endh = get_files_range(files)
nhour = endh - starth + 1

WP = np.empty((nhour, ngrid))
SP = np.empty((nhour, ngrid))

outfile_wind =  'data/rawdata/UNFILTERED_hourly_WP.npy'
outfile_solar = 'data/rawdata/UNFILTERED_hourly_SP.npy'

# Convert wind to power.  Expected variables are "SP CF", "WP CF",
# "lat_lon_index", "time" (lat_lon_index may not be used)
for nfile in files:
    print(nfile)
    with netcdf_file(nfile,'r') as incdf:
        time = incdf.variables['time']

        # Convert 2D wind speed to 1D.
        uadd_offset = incdf.variables['u100'].add_offset
        uscale_factor = incdf.variables['u100'].scale_factor
        vadd_offset = incdf.variables['v100'].add_offset
        vscale_factor = incdf.variables['v100'].scale_factor
        file_start = time[0] - starth
        file_end = time[-1] - starth + 1
        file_nhour = time.shape[0]
        # clear references to prevent netcdf warning message
        time = None

        WP[file_start:file_end,:] = \
            np.sqrt((incdf.variables['u100'][:,:].astype(float)
                     .reshape(file_nhour, ngrid) * uscale_factor + uadd_offset)**2
                  + (incdf.variables['v100'][:,:].astype(float)
                     .reshape(file_nhour, ngrid) * uscale_factor + uadd_offset)**2)
        #
        # solar
        #
        var = incdf.variables['ssrd']
        SP[file_start:file_end, :] = var[:,:].astype(float).reshape(file_nhour, ngrid) \
            * var.scale_factor + var.add_offset
        var = None


# Convert J m**-2 to solar capacity factor (equations in figure S19,
# but temperature variation ignored so it reduces to linear scale
# factor).
cap_factor_scale = 0.00000024
SP *= cap_factor_scale

print("Applying power curve. Takes 10(?) minutes.")
rescale = 0.0005  # fudge factor to match original paper data
WP = my_polyval(WIND_POWER_COEFFICIENTS, WP) * rescale

print("Saving merged data to " + outfile_wind)
np.save(outfile_wind, WP)
print("Saving merged data to " + outfile_solar)
np.save(outfile_solar, SP)
