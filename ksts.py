# KSTS source code for 2024 NMSU paper
# Usage: ksts.py [first-simulation-index] [number-simulations]

# Notes from original paper:
# D_t is is the windowed feature vector.  It has dimension, d, which is the
#   number of lagged values which the successor (aka simulated result)
#   depends upon.  In our case this is X_t.
# D_i is the _current_ feature vector, for time, i.  It also has dimension, d.
#   In our case this is xtest (Xnew[i])

# The algorithm finds the historical nearest neighbors to the to the current feature
#   vector then resample from their successors.
#   In our case the successors are Y

# lags of (1, 2) corespond to dependences on t-1 and t-2 values but for the 2024 paper we used only one lag.

# Bug in the published R code from 2022 Amonkar et al paper
#   The embedded structure has "anti-lagged" values instead of lagged values.
#      The values are t+1, t+2, etc instead of t-1, t-2.

import sys
import os
import time
import traceback
import threading
from concurrent import futures
from datetime import datetime, timedelta
import calendar
import math
import cProfile
import pstats
from pstats import SortKey
import psutil

import numpy as np
from numpy.lib.stride_tricks import as_strided
from tqdm import tqdm
try:
    import cupy as cp
    from cupyx.profiler import benchmark
    nm = cp  # sets module for other numerical computation
    using_gpu = True
except:
    # no GPU available
    nm = np
    using_gpu = False
    print("USING CPU, NOT GPU")

im = np  # sets the module used for window indexing structures
index_dtype = 'int32'
float_dtype = 'float32'

HISTORICAL_HOURLY_WIND_FILE = 'data/UNFILTERED_hourly_WP.npy'
HISTORICAL_HOURLY_SOLAR_FILE = 'data/UNFILTERED_hourly_SP.npy'
DEBUG = False

def log_memory(label):
    process = psutil.Process(os.getpid())
    mem = process.memory_info().rss
    print("Max resident CPU memory %s %.3f GB" % (label, mem / 1024**3))
    if nm != np:
        p = cp.get_default_memory_pool()
        t = p.total_bytes() / 1024**3
        u = p.used_bytes() / 1024**3
        print("GPU mem stats: %.2f GB reserved in pool, %.2f GB allocated from pool"
              % (t, u))
        print("stream", cp.cuda.get_current_stream())
        # pp = cp.get_default_pinned_memory_pool()
        # f = pp.n_free_blocks()
        # print("GPU pinned free blocks: %d" % f)
    print("%d threads are active" % threading.active_count())
    sys.stdout.flush()


def log_variable_size(var, label):
    try:
        print("%s shape: %s %s" % (label, var.shape, var.dtype))
    except:
        pass
    print("%s using %.1f MB" % (label, var.nbytes/1024**2))
    sys.stdout.flush()


def drop_leap_days(Fld, nlag_day, periods_per_day, variable_name):
    # Transform input data to have 365 days in every year plus nlag
    # sacrificial days for initial conditions.
    ntime_leap = Fld.shape[0]
    nday_leap = int(ntime_leap / periods_per_day)
    if ntime_leap != nday_leap * periods_per_day:
        h = ntime_leap - nday_leap * periods_per_day
        print("Error: hourly data set contains a partial day with %d hours" % h)
        raise Exception("Error: hourly data set contains a partial day with %d hours" % h)
    nyear = int(nday_leap / 365)
    nday_noleap = nyear * 365 + nlag_day
    nday_to_drop = nday_leap - nday_noleap
    if nday_to_drop == 0:
        return Fld
    print("Dropping %d leap days from %s input data" % (nday_to_drop, variable_name))
    start = nday_leap / (nday_to_drop + 1)
    idx = np.round(np.linspace(start, nday_leap - 1, nday_to_drop)).astype(int)

    drop_indices = np.array([], dtype='int')
    for i in range(periods_per_day):
        drop_indices = np.append(drop_indices, idx + i)
    drop_indices = sorted(drop_indices)

    assert len(drop_indices) == nday_to_drop * periods_per_day

    # cupy is missing the delete function so use three steps to delete.
    all_indices = np.indices((Fld.shape[0],))[0]
    assert len(all_indices) == Fld.shape[0]
    keep_mask = np.isin(all_indices, drop_indices, invert=True)
    # boolean index did not match indexed array along dimension 0; dimension is 552264 but corresponding boolean dimension is 1
    Fld = Fld[keep_mask]

    expected_length = ntime_leap - len(drop_indices)
    if Fld.shape[0] != expected_length:
        print("Error: difference sanity check failed after dropping leap days %d %d"
              % (Fld.shape[0], expected_length))

    expected_length = (nyear * 365 + nlag_day) * periods_per_day
    if Fld.shape[0] != expected_length:
        print("Error: additive sanity check failed after dropping leap days %d %d"
              % (Fld.shape[0], expected_length))
    return Fld


def apply_window(X, Y, X_t, Y_t, window, window_size, i, period_index, day_mv, max_embd, periods_per_day, ntime):
    # to benchmark
    j = i - max_embd * periods_per_day
    compute_window(j, window, period_index, day_mv, periods_per_day)
    assert window.min() >= 0
    assert window.max() < X.shape[0]
    assert window.max() < Y.shape[0]
    wsize = int(im.sum(window))
    if  wsize != window_size:
        # start with NaNs because not all windows fill the arrays (due
        # to exclusion of first max_embd time points).
        X_t[:,:,:] = nm.nan
        Y_t[:,:,:] = nm.nan
        if abs(window_size - wsize) > 1:
            print('Oops.  Computed window has size of %d but there is room for %d' % (wsize, window_size))
    if len(window) > X.shape[0]:
        print("Simulation has %d periods but window mask is %d long.  Assuming validation and truncating."
              % (X.shape[0], len(window)))
        window = window[:X.shape[0]]

    if nm == np:
        if periods_per_day == 24:
            # Sum 24 hours instead of using zero hour.
            # window is a boolean vector with length same as X and Y.
            # ww contains window's indices.
            ww = np.where(window)[0]
            # could pre-calculate this (using reshape to vectorize)
            for i, wi in enumerate(ww):
                X_t[i,:,:] = X[wi:wi+24].sum(axis=0)  # for distance calc (Allows weighted distance at each lag.)
                Y_t[i,:,:] = Y[wi:wi+24].sum(axis=0)  # results picked from Y_t (which contains time-successor values)
        else:
            # Simulating with daily data.
            # compare value at zero hour
            X_t[:,:,:] = X[window,:,:]  # used for distance calculations (Allows weighted distance for different lags.)
            Y_t[:,:,:] = Y[window,:,:]  # result values pulled from this matrix (which contains time-successor values)
    else:
        # move windowed data to GPU memory
        if periods_per_day == 24:
            # Sum 24 hours instead of using zero hour.
            ww = np.where(window)[0]
            for wi in ww:
                X_t[wi,:,:] = cp.asarray(X[wi:wi+24].sum(axis=0))
                Y_t[wi,:,:] = cp.asarray(Y[wi:wi+24].sum(axis=0))
        else:
            # Simulating with daily data.
            # compare value at zero hour
            X_t[:,:,:] = cp.asarray(X[window,:,:])
            Y_t[:,:,:] = cp.asarray(Y[window,:,:])

#______________________________________________________________________________#
### KSTS Algorithm Functions (translated from R) ###

### Function 1 
# Objective - Moving Window Indices - Returns the day/hour index of interest.
### Inputs
#   1. Current Day-of-Year (curr)
#   2. Max value of Day-of-Year (max)
#   3. Moving Window Size (window)
### Output
#   1. All DOYs in the moving window

def close_ind(curr_period, last_period, window_in_days, periods_per_day):
    # curr_period - index of window center period
    # last_period - index of last period in the time range
    # window_in_days - 1/2 of window length
    # periods_per_day - 1 or 24 for daily or hourly resolution
    
    # Return 2x window with wrapping plus the curr_period in the
    # middle.  These are period numbers, not indices, ie first period
    # is 1.
    assert curr_period <= last_period
    window_in_periods = window_in_days * periods_per_day
    if((curr_period - window_in_periods) <= 0):
        # print('path 1')
        # wrap around

        # so all windows hitting last day of year include leap days and others not????
        # print("last_period", last_period)
        a = im.arange(last_period + curr_period - window_in_periods, last_period + 1, periods_per_day, dtype=index_dtype)
        start_period = (curr_period - 1) % periods_per_day + 1
        b = im.arange(start_period, curr_period + window_in_periods + 1, periods_per_day, dtype=index_dtype)

        result = im.append(a, b)

        assert len(result) == 2 * window_in_days + 1
        return result
    if (curr_period + window_in_periods) > last_period:
        # print('path 2')
        # wrap around
        start_period = (curr_period - 1) % periods_per_day + 1
        result = im.append(im.arange(curr_period - window_in_periods, last_period + 1, periods_per_day, dtype=index_dtype),
                           im.arange(start_period, window_in_periods - last_period + curr_period + 1, periods_per_day, dtype=index_dtype))
        assert len(result) == 2 * window_in_days + 1
        return result
    # print('middle path')
    # window does not wrap at either end.
    result = im.arange(curr_period - window_in_periods,
                       curr_period + window_in_periods + 1, periods_per_day, dtype=index_dtype)
    # print(result)
    assert len(result) == 2 * window_in_days + 1
    return result

def compute_window(j, window, period_index, window_in_days, periods_per_day):
    # Select a window of nearby periods to consider.

    # period indices are 1-based indices.  j is zero-based.
    period_of_year = int(period_index[j])
    # assume leap days have been discarded
    last_period = max(period_index)

    # window_periods is on an annual basis
    window_periods = close_ind(period_of_year, last_period, window_in_days, periods_per_day)

    #
    # Subset to the time window.
    #
    # Exclude the current time period (0 < 1) so will not match because
    # not a valid day of year)
    # print("Excluding this hour: %d" % filtered_periods[i])
    # Avoid copying by saving the value to restore later.
    this_period_saved = period_index[j]
    period_index[j] = 0

    # collect same periods across multiple years
    window[:] = im.isin(period_index, window_periods)
    # restore "masked" value
    period_index[j] = this_period_saved
    # print("len proposed window: %d" % len(window_periods))
    # proposed_mask = im.isin(period_index, window_periods)
    # proposed_indices = np.where(proposed_mask)[0]
    # filtered_indices = np.where(window)[0] + 48
    # print(window_periods.min(), periods.min())
    # print(proposed_indices[:10])
    # print(filtered_indices[:10])
    # print("proposed vs filtered", len(proposed_indices) - len(filtered_indices))
    # print(len(filtered_indices), np.sum(window))
    # print(periods[window][:61])
    assert len(window) == len(period_index)
    return window

def calc_period_index(periods_per_day, ntime):
    # period indices are 1-based indices.
    nyear = int(ntime / periods_per_day / 365)
    calc_ntime = nyear * periods_per_day * 365
    if calc_ntime != ntime:
        if calc_ntime == 0:
            print("Input has less than a year of data.  Assuming validation mode.")
            calc_ntime = periods_per_day * 365
            calc_ntime = ntime
        else:
            # leap days not dropped ?
            print("Error: windower found %d too many time periods" % (ntime - calc_ntime))
            print(calc_ntime, ntime, periods_per_day, nyear)
            exit(1)
    result = im.zeros(calc_ntime, dtype=index_dtype)
    i = 0
    while i < calc_ntime:
        # number of periods in this year
        # nperiod = (365 + calendar.isleap(year)) * periods_per_day
        # assuming leap days have been dropped
        nperiod = 365 * periods_per_day
        if i + nperiod <= calc_ntime:
            result[i:i + nperiod] = im.arange(1, nperiod + 1, dtype=index_dtype)
        else:
            if DEBUG:
                print("At end of years filling in %d more" % (ntime - i))
            result[i:] = im.arange(1, ntime - i + 1, dtype=index_dtype)
        i += nperiod
    return result
    
def knn_all_sites(Xnew_w, X_t, lags, nneib, weights, nn_index):
    # Xnew_w: results from previous time step, windowed.
    ngrids = X_t.shape[2]
    for j in range(ngrids):
        # Set the 'test' parameter.  Neighbor distances are calculated
        # from the lagged values in already-calculated simulation
        # results (from previous time step).

        #   In practice cur_lags is (1,0), (2,1) ...
        #     (iterations start at i==2, sel_lags is (1, 2))
        #   In practice, weights are (1, 0) so only the first value of xtest is used.
        #   So the sequence is testing distance to values from indices: 1, 2, 3 ...
        #   while the time index runs, 2, 3, 4 ...
        #   So the value used from xtest is the t-1 lagged simulation result.
        xtest = Xnew_w[lags, j]

        # Run the KNN algorithm for this site.  It returns day indices
        # of the N nearest neigbors in time.
        nn_index[j,:] = knn_index(X_t[:,:,j], xtest, nneib, weights)


# invocation would be like knn_index_2D[grid, blocks](x, y, size)
#  Size of grid in blocks
#  Dimension of each thread block
# @jit.rawkernel()
# not the way to do this.  Has to be written in basic math operations
def knn_index_2D(d, K):
    # Vectorized knn_index which runs 4x faster than version with loop

    # Partition each grid on Kth largest distance.  Then drop indices
    # to values with larger distances.

    ind = nm.argpartition(d, K - 1, axis=0)[:K]

    if nm == np:
        # Unlike numpy.argpartition, cupy.argpartition fully sorts the array.
        # Sort the (remaining) top K distances for each grid point.
        args = nm.argsort(nm.take_along_axis(d, ind, axis=0), axis=0)
        
        # Return indices into original d by indexing ind.
        return nm.take_along_axis(ind, args, axis=0)
    return ind

def knn_all_sites_2D(Xnew_w, X_t, lags, K, weights, nn_index):
    # Xnew_w: results from previous time step, windowed.  For hourly
    #         mode, every hour of each lagged day is included.
    # X_t: The observed values at current time step.  All sites, all lags.
    #      For hourly mode, includes 24-hours per day.
    ngrids = X_t.shape[2]
    n_features = len(lags)
    if n_features == 2 and weights[1] == 0 and weights[0] == 1:
        # ds = (x[:,0] - xtest[0])**2
        # ds = nm.linalg.norm(x - xtest, axis=1)
        # optimization

        # First dim of ds is value at each window point. Second dim is site (ngrid).
        ds = np.abs(X_t[:,0] - Xnew_w[lags[0]])
    else:
        global knn_d
        if knn_d is None:
            # optimization.  (Where weight is zero the initial zeros
            # continue to be zero.)
            n_samples = x.shape[0]
            knn_d = nm.zeros((n_samples, n_features))

        # Euclidean weighted distance for each feature
        for i, w in enumerate(weights):
            if w != 0.:
                knn_d[:,i] = w * nm.abs(x[:,i] - xtest[i])

        # Sum across features
        ds = knn_d.sum(axis=1)

    # could speed up by eliminating transpose?
    nn_index[:] = knn_index_2D(ds, K)

### Function 2 
# Objective - Compute the K-Nearest Neighbors for each site
### Inputs
#   1. All the feature vectors (x)
#   2. Current feature vector (xtest)
#   3. Number of Neighbors (nneib)
#   4. Scaling Weights (weights)
### Outputs
#   1. Time indices corresponding to the k-nearest neighbors 

knn_d = None
def knn_index(x, xtest, K, weights):
    n_features = len(xtest)
    if n_features == 2 and weights[1] == 0 and weights[0] == 1:
        # ds = (x[:,0] - xtest[0])**2
        # ds = nm.linalg.norm(x - xtest, axis=1)
        # optimization
        ds = nm.abs(x[:,0] - xtest[0])
    else:
        global knn_d
        if knn_d is None:
            # optimization.  (Where weight is zero the initial zeros
            # continue to be zero.)
            n_samples = x.shape[0]
            knn_d = nm.zeros((n_samples, n_features))

        # Euclidean weighted distance for each feature
        for i, w in enumerate(weights):
            if w != 0.:
                knn_d[:,i] = w * nm.abs(x[:,i] - xtest[i])

        # Sum across features
        ds = knn_d.sum(axis=1)

    # Return indices of K "closest" days. (Truncate to neighbor
    # count.)
    return nm.argsort(ds, kind='quicksort')[:K]

def topk_by_partition(input, k, axis=None, ascending=True):
    if not ascending:
        input *= -1
    ind = nm.argpartition(input, k, axis=axis)
    ind = nm.take(ind, nm.arange(k, dtype=index_dtype), axis=axis) # k non-sorted indices
    input = nm.take_along_axis(input, ind, axis=axis) # k non-sorted values

    # sort within k elements
    ind_part = nm.argsort(input, axis=axis)
    ind = nm.take_along_axis(ind, ind_part, axis=axis)
    if not ascending:
        input *= -1
    val = nm.take_along_axis(input, ind_part, axis=axis) 
    return ind, val

def knn_numpy(inputs, test_instance, k):
    # an experiment.  Not used
    # according to https://sijanb.com.np/posts/optimizing-k-nearest-neighbors-knn-algorithm-in-python/
    #  numba provides a 2-3x speedup
    
    # calculate L2 norm between all training points and given test_point
    # inputs['distance'] = nm.linalg.norm(inputs.values-test_instance.values, axis=1)
    distance = nm.abs(inputs - test_instance)
    # sort based on distance
    # inputs = inputs.sort_values('distance', ascending=True)
    sort_index = nm.argsort(distance)
    
    # pick k neighbors
    neighbors = inputs.head(k)

    # get list from dataframe column
    classes = neighbors['CLASS'].tolist()

    # create counter of labels
    majority_count = Counter(classes)
    return majority_count.most_common(1).pop()[0]
    

pos_neg_fp = None
allprob_alloced = False
def pick_one_index(nn_index, nneib, result, validation_mode, i):
    # over a second spent here!

    def sum_probs_looped(unique_index, nn_index, nneib, unique_prob):
        # Compute the resampling probability

        nn_index_flat = nn_index.reshape((-1,))  # flatten to simplify similarity scoring
        for k in range(len(unique_index)):
            # 'where()' returns a vector of indices into the flattened nn_index.
            #  The "modulo nneib" turns each index into a neighbor distance rank for this day.
            #   This means the further away the neighbors, the _larger_ the value.
            #   So take the inverse to score this day for overall closeness to neighbors.

            # Add one to the indices to make up for zero-based indexing
            temp = 1. / (((nm.where(nn_index_flat == unique_index[k])[0]) % nneib) + 1)

            # Add up probabilies/scores for this single day across all
            # sites. This step defines the KSTS algorithm.  uniq_prob now
            # holds unnormalized cross-site simularity.
            unique_prob[k] = nm.sum(temp)


    def mask_indices(nn_index, unique_index):
        nn_index_flat = nn_index.reshape((-1,))  # flatten to simplify similarity scoring
        N = len(unique_index)
        M = len(nn_index_flat)
        mask = nm.empty((N, M), dtype=bool)
        for k in range(N):
            mask[k] = nn_index_flat != unique_index[k]
        return mask

    if nm != np:
        # This kernel bypasses making a big probability matrix.
        cp_mask_indices = cp.ElementwiseKernel(
            'raw T nflat, raw T K, raw T flat, raw T u_index',
            'bool result',
            # 'float32 result',
            'result = flat[i % nflat] != u_index[i / nflat]',
            # 'result = float(flat[i % nflat] == u_index[i / nflat]) / float((i % K) + 1)',
            # reduce could do sum.  Use size parameter to state map result shape?
            # 'a[i / nflat] + b[i]'
            'cp_mask_indices')

    def sum_probs_vectorized(unique_index, nn_index, nneib, unique_prob):
        # This vectorized version is faster but requires approximate
        # 1GB of memory per simulation.
        global allprob_alloced
        #
        # Compute the resampling probability
        #
        N = unique_index.size
        M = nn_index.size
        L = int(M / nneib)  # ngrid
        assert L == nn_index.shape[0]
        assert nneib == nn_index.shape[1]
        if nm == np:
            mask = mask_indices(nn_index, unique_index)
        else:
            mask = cp_mask_indices(M, nneib, nn_index, unique_index, size=N * M).reshape(N, M)

        # allprob uses 1GB of memory per simulation! but allows a nice GPU speedup.
        # Add one to the indices to make up for zero-based indexing.

        # These probabilities are assigned to neighbors based upon
        # neighbor rank.
        #
        # This resampling kernel is the same across all time points
        # and across all sites and could be pre-computed and stored
        # before simulation. It is a function of the number of
        # neighbors k and _not_ the distances.  Here a larger version
        # is calculated but it will later be truncated during
        # truncation to K neighbors.
        allprob = nm.broadcast_to(nm.tile(1. / (nm.arange(nneib, dtype=float_dtype) + 1), L), (N, M)).copy()

        # zero out the sites beyond neighbor limit (at a grid site).
        allprob[mask] = 0
        if not allprob_alloced:
            allprob_alloced = True
            log_variable_size(allprob, "allprob")
            log_memory("allprob allocation")
        # This vectorized sum seems to be key to a big GPU
        # acceleration.  It comes at the cost of the large memory
        # allocation for allprob.
        unique_prob[:] = nm.sum(allprob, axis=1)


    def sum_probs_gpu(unique_index, nn_index, nneib, unique_prob):
        all_probs = cp.ElementwiseKernel(
            'raw T K, raw T flat, raw T u_index',
            'float32 result',
            'result = float(flat[i % flat.size()] '
            '== u_index[i / flat.size()]) / float((i % K) + 1)',
            'all_probs')

        N = unique_index.size
        M = nn_index.size
        allprob = all_probs(nneib, nn_index, unique_index, size=N*M).reshape(N, M)
        unique_prob[:] = nm.sum(allprob, axis=1)


    def sum_probs_broadcast(unique_index, nn_index, nneib, unique_prob):
        assert nn_index.shape[-1] == nneib
        N = len(unique_index)

        mask = nm.empty([N] + list(nn_index.shape), dtype=bool)
        for k in range(N):
            mask[k] = nn_index == unique_index[k]
        allprob = 1. / (nm.arange(nneib, dtype=float_dtype) + 1)

        nm.sum((mask * allprob).reshape(N, -1), axis=1, out=unique_prob)


    def kernel(unique_index, nn_index, nneib, unique_prob):
        assert nn_index.shape[1] == nneib
        N = len(unique_index)
        M = nn_index.shape[0]  # ngrid
        L = nn_index.shape[1]  # neighbor index into timepoint
        for i in range(N):
            # calulate unique_prob[i]
            psum = 0.
            ui = unique_index[i]
            # P = 1. / (cp.arange(L) + 1)
            # unique_prob[i] = cp.sum(P * (ui == nn_index))
            for k in range(L):
                p = 1. / (k + 1)
                # psum_v += p * (ui == nn_index[:,k])
                for j in range(M):
                    if ui == nn_index[j,k]:
                        psum += p
            unique_prob[i] = psum
            

    #
    # Compute the aggregated similarity vector unique_prob (aka S_t in literature)
    #
    unique_index = nm.unique(nn_index)  # flat vector across all rows
    unique_prob = nm.empty(len(unique_index), dtype=float_dtype)  # empty similarity vector
    if nm == np:
        # While unique_index is sorted by index, this func seems to
        # assign probabilities by the original order (in nn_index)
        sum_probs_vectorized(unique_index, nn_index, nneib, unique_prob)
    else:
        sum_probs_gpu(unique_index, nn_index, nneib, unique_prob)
    #
    # Curtail length to neighbor count then rescale/normalize the
    # resampling probablites to produce the final scaled/normalized
    # S_t similarity vector (pj below).
    #
    PROB = 0
    INDEX = 1
    pj = nm.stack((unique_prob, unique_index))
    thresh = nm.sort(unique_prob)[-nneib]
    pj = pj[:,pj[PROB] >= thresh]
    pj[PROB] = pj[PROB] / nm.sum(pj[PROB])  # normalize

    # pj.unique_prob holds the curtailed similarity vector, S_t.
    #  (Corresponding time indices are in pj.unique_index.)

    # Use the curtailed similarity vector to pick a neighbor value for
    # all sites, simultaneously, for this time point.
    if validation_mode:
        # order of unique indices is different between python and R
        ns = int(pj[INDEX].min())  # could do better
    else:
        ns = int(nm.random.choice(pj[INDEX], size=1, p=pj[PROB])[0])
    result[0] = ns

diagfp = None
def pick_and_save_timestep(nn_index_T,
                           Xnew_w,
                           Y_t,
                           nneib,
                           one_index,
                           Y,
                           window,
                           outfp,
                           i,
                           validation_mode,
                           periods_per_day):
    # one_index: is a single-element array (because it is used to
    #            return value by reference).
    # nn_index_T: ngrid x K, K closest timepoints for each site
    pick_one_index(nn_index_T, nneib, one_index, validation_mode, i)

    # ns was found from nearest neighbors to lagged current feature vector, xtest.
    # successor/result value is the value at ns in the not-lagged historical
    # data.
    # "Pull the successor from historical data at ns (and assign that to Xnew).
    #  in another place the wording is "resample across all sites from the time
    #  index which correponds to the _next_ time step of the selected from S_t.
    #  This becomes the simulation data at time t + 1"

    # Store picked values to last lag entry for use in finding distance
    # for next iteration.
    Xnew_w[-1] = Y_t[one_index[0]][0]

    # one_index is in the space of the windowed data
    reframed_one_index = np.where(window)[0][one_index[0]]
    if periods_per_day == 24:
        # Save 24-hour results of this timestep.
        for j in range(reframed_one_index, reframed_one_index + 24):
            outvec = Y[j][0]
            if nm == np:
                arrstr = np.char.mod('%.4f', outvec)
            else:
                arrstr = np.char.mod('%.4f', cp.asnumpy(outvec))
        outfp[0].write(','.join(arrstr) + '\n')
    else:
        outvec = Y[reframed_one_index][0]
        if nm == np:
            arrstr = np.char.mod('%.4f', outvec)
        else:
            arrstr = np.char.mod('%.4f', cp.asnumpy(outvec))
        outfp[0].write(','.join(arrstr) + '\n')

# The following checkpoint support functions work on distributed
# compute cluster running SLURM.
def checkpoint_file(sim_i):
    ddir = '/fs1/scratch/home/checkpoint/'
    if HOURLY:
        ddir = './hourly_results/'
    else:
        ddir = './daily_results/'
    cp_fname = 'checkpoint_%d.npy' % sim_i
    if not os.path.exists(ddir):
        ddir = './'
    return ddir + cp_fname


def clear_checkpoint(sim_i):
    os.remove(checkpoint_file(sim_i))


def save_checkpoint(sim_i, i, Xnew_w, data_fp):
    fname = checkpoint_file(sim_i)
    with open(fname, 'wb') as fp:
        nm.save(fp, i)
        nm.save(fp, data_fp.tell())
        nm.save(fp, Xnew_w)


def restore_checkpoint(sim_i, start_i, Xnew_w):
    cp_fname = checkpoint_file(sim_i)
    ddir = os.path.dirname(cp_fname)
    if not ddir:
        ddir = '.'
        rdir = '.'
    else:
        # dir above checkpoint dir
        rdir = os.path.dirname(ddir)
        rdir = rdir if rdir else '.'
    result_fname = rdir + '/simout_%d.csv' % sim_i
    if os.path.exists(cp_fname):
        with open(cp_fname, 'rb') as fp:
            new_i = int(nm.load(fp))
            seekpos = nm.load(fp)
            Xnew_w = nm.load(fp)
        outfp = open(result_fname, 'r+')
        outfp.seek(seekpos)
        start_i = new_i
        print("Restarting simulation %d from checkpoint at %d" % (sim_i, start_i))
    else:
        outfp = open(result_fname, 'w')
    return start_i, [outfp]


### Function 3
# Objective - KSTS Simulator

###Input
#   1. Concatenated Data (Fld)
#   2. Number of Grid Points (ngrids)
#   3. Record Length (N_times)
#   4. Number of Nearest Neighbors (nneib)
#   5. Scaling Weights (weights)
#   6. Record Start Date (start_date)
#   7. Moving Window Size (day_mv)
#   8. Maximum Embedding (max_embd)
#   9. Selected Embedding Lags (sel_lags)
#   10. Number of selected lags  (n_lags)

###Output
#   1. A single KSTS Simulation Realization

def kwrap(*args, **kwargs):
    try:
        ksts(*args, **kwargs)
    except Exception as e:
        print(e)
        traceback.print_exc()

# KNN with embeddings without climate 
def ksts(Fld, ngrids, N_times, # Data Parameters
         simulation_idx,
         nneib, weights, # KNN Parameter
         start_year, day_mv,  # Date Seasonality Parameters
         max_embd, sel_lags, n_lags, # Embedding Parameters
         validation_mode=False,
         hours_per_period=24):
  if DEBUG:
        print("%d hours per period" % hours_per_period)
  if nm != np:
      stream = cp.cuda.Stream(non_blocking=True)
      stream.use()
      print("sim %d using stream %s" % (simulation_idx, stream))
      sys.stdout.flush()
  
  # Period Indices.  Constant throughout the simulation.
  if hours_per_period == 1:
      periods_per_day = 24
  elif hours_per_period == 24:
      periods_per_day = 1
  period_index = calc_period_index(periods_per_day, N_times - max_embd * periods_per_day)

  # Xnew_w contains the simulation state, ie the lagged results from
  # previous timesteps used as inputs for each new timestep.
  if True:
      # window of Xnew
      Xnew_w = nm.empty((max_embd * periods_per_day, ngrids), dtype=float_dtype)
  else:
      Xnew = nm.empty((N_times, ngrids), dtype=float_dtype)
      Xnew[:,:] = nm.nan
      print("Xnew size: %.2f GB" % (Xnew.size * Xnew.itemsize/1024**3))

  # In order to have lagged values to use as input as feature vectors
  # we have to discard the first max_embd values from the lag=0 data
  # set.  This means the simulation must start at t=max_embd.

  # Here we fabricate result values for t<max_embd by jittering the
  # historical data at those time points. These jittered values may
  # appear in final output because these time points have no result
  # from KNN selection because these time indices are skipped in the
  # main loop.  Max value is around 1364085 aand jitter figures noise
  # from that.

  for i in range(max_embd * periods_per_day):
      if validation_mode:
          if nm == np:
              Xnew_w[i] = Fld[i]
          else:
              Xnew_w[i] = cp.asarray(Fld[i])
      else:
          Xnew_w[i] = jitter(Fld[i], factor=1)  # add a bit of noise
  
  # Create the feature vector/state space: X and Y.  These variables
  # are constant throughout the simulation.

  ntime_X = N_times - max_embd * periods_per_day
  X = np.empty((ntime_X, n_lags, ngrids), dtype=float_dtype)
  Y = np.empty((ntime_X, 1, ngrids), dtype=float_dtype)
  if True:
      # simpler lag structure creation

      # zero column starts at i==2
      lag_gap = max_embd * periods_per_day
      Y[:,0,:] = Fld[lag_gap:,:]
      for i, j in enumerate(sel_lags):
          f = lag_gap - j
          X[:,i,:] = Fld[f:-j,:]
  else:
      for j in range(ngrids):
          # Get lagged structure up to max embedding.  Result of embedding
          # has max_embd + 1 (3 in practive) columns.  Each column has
          # different time lag embedded.  Column 0 has lag=0 (after
          # discarding initial values), then lag=1, lag=2, etc.

          x_fld = embed_orig(Fld[:,j], max_embd + 1)
    
          # In practice, sel_lags selects t-1 lagged and t-2 data sets.
          # Weights then narrows that to t-1.
          # X is D_t in paper.
          X[:,:,j] = x_fld[:, sel_lags]
          # Y is no-lag historical data (with first max_embd timepoints dropped).
          Y[:,0,j] = x_fld[:,0]

  if DEBUG:
      print('weights', weights)
      print('lags', sel_lags)
  # print('X')
  # for r in range(X.shape[0]):
  #     print(X[r,:,0])
  # print('Y')
  # print(Y[:,0,0])
  jittered = list(range(max_embd))
  if DEBUG:
      print('Jittered initialized indices: ', jittered)
      print('First iter index: ', max_embd)
  # print('jittered Xnew: ', Xnew[jittered])
  # Run the simulation
  rng = nm.random.default_rng()
  # All but the window centered on the current point have day_mv * 2 + 1 time points.
  window_size = int(N_times / periods_per_day / 365) * (day_mv * 2 + 1) - 1
  if validation_mode:
      # validation typically uses less than a year of data
      window_size = day_mv * 2

  #
  # Variables initialized in the main loop (per iteration)
  #
  nn_index = nm.empty((nneib, ngrids), dtype=index_dtype) # all the nearest neighbors as time indices
  X_t = nm.empty((window_size, n_lags, ngrids), dtype=float_dtype)
  Y_t = nm.empty((window_size, 1, ngrids), dtype=float_dtype)
  window = nm.empty(N_times - n_lags * periods_per_day, dtype=bool)

  # in case we are resuming a suspended simulation ...
  start_i = max_embd * periods_per_day
  start_i, outfp = restore_checkpoint(simulation_idx, start_i, Xnew_w)

  iters = 0
  startTime = datetime.now()
  j = 1
  niter = N_times - max_embd * periods_per_day
  using_slurm = os.environ.get("SLURM_ARRAY_TASK_ID") is not None

  for i in tqdm(range(start_i, N_times, periods_per_day),
                position=simulation_idx,
                disable=using_slurm):  # i: time index
    if j % 100 == 0:
        now = datetime.now()
        elapsed_hours = (now - startTime).total_seconds()/60/60
        expected_total = niter * elapsed_hours / j
        expected_remaining = (niter - j) * elapsed_hours / j
        with tqdm.external_write_mode():
            print("%s %6d %6d %.2f %.2f" % (str(datetime.now()), i, niter, expected_remaining, expected_total))
            sys.stdout.flush()
        if j % 5000:
            outfp[0].flush()
            save_checkpoint(simulation_idx, i, Xnew_w, outfp[0])
    j += 1
    if iters == 1:
        log_memory("second iter")
        log_variable_size(Xnew_w, "Xnew_w")
        log_variable_size(nn_index, "nn_index")
    if iters == 99:
        log_memory("100th iter")
        log_variable_size(Xnew_w, "Xnew_w")
        log_variable_size(nn_index, "nn_index")
    iters += 1

    apply_window(X, Y, X_t, Y_t, window, window_size, i, period_index, day_mv, max_embd, periods_per_day, len(X))

    # Run the KNN algorithm for each site
    cur_lags = 0 - sel_lags
    if True:
        # benchmarked at around 2 ms
        # print(benchmark(knn_all_sites_2D, (Xnew, X_t, cur_lags, nneib, weights, nn_index)))
        knn_all_sites_2D(Xnew_w, X_t, cur_lags, nneib, weights, nn_index)
    else:
        nn_index = knn_all_sites(Xnew_w, X_t, cur_lags, nneib, weights)


    # Because index selection uses an array of unique indices so each
    # simulation can have different number of these unique indices,
    # the index selection is in a loop which cannot be vectorized.
    # Possibly it could be parallelized.

    Xnew_w[:-1] = Xnew_w[1:]
    one_index = np.empty(1, dtype='int')
    nn_index_T = nn_index.T
    pick_and_save_timestep(nn_index_T, Xnew_w, Y_t, nneib, one_index, Y, window, outfp, i, validation_mode, periods_per_day)

  # Break out wind and solar result data sets.
  n_site = ngrids // 2
  for f in outfp:
      f.close()
  clear_checkpoint(simulation_idx)
  print("Output files closed")
  sys.stdout.flush()


def embed_orig(x, ncol):
    """
    Each row consists of x[t], x[t - 1], …, x[t - ncol +1],
    where t is the original index of x
    """
    nrow = len(x) - ncol + 1
    assert nrow > 0
    result = nm.empty((nrow, ncol), dtype=float_dtype)
    for j, k in enumerate(range(ncol - 1, nrow + ncol - 1)):
        result[j,:] = x[range(k, k - ncol, -1)]
    return result


def embed_rotated(x, ncol):
    # not used
    """
    Each row consists of x[t], x[t - 1], …, x[t - ncol +1],
    where t is the original index of x
    """
    nrow = len(x) - ncol + 1
    assert nrow > 0
    result = nm.empty((nrow, ncol), dtype=float_dtype)
    result[:,0] = x[-nrow:]
    for j in range(1, ncol):
        result[:,j] = x[ncol-j-1:-j]
    return result


def embed(x, ncol):
    """
    Each row consists of x[t], x[t - 1], …, x[t - ncol +1],
    where t is the original index of x.
    (fully vectorized version)
    """
    assert isinstance(x, nm.ndarray)
    nrow = len(x) - ncol + 1
    assert nrow > 0
    x = nm.flip(x)
    strides = x.strides[0]
    return nm.flip(as_strided(x, shape=(nrow, ncol), strides=(strides, strides)),
                   axis=0)


def jitter(x, factor = 1, amount = None, nmod=nm):
    # Implemented based on
    # https://github.com/wch/r-source/blob/5a156a0865362bb8381dcd69ac335f5174a4f60c/src/library/base/R/jitter.R
    # Tejus <tejzpr@gmail.com>
    if len(x) == 0:
        return x

    z = max(x) - min(x)
    if z == 0:
        z = min(x) if min(x) > 0 else 1

    if amount == None:
        unsorted = [nmod.round(elem, 3 - math.floor(math.log10(z))) for elem in x]
        xUniqs = nmod.unique(unsorted)
        d = nmod.diff(xUniqs)
        if len(d) > 0:
            d = min(d)
        else:
            d = z/10
        amount = factor/5 * abs(d)
    elif amount == 0:
        amount = factor * (z/50)

    K = nmod.random.uniform((-1*amount),amount,(len(x),))
    return nmod.array([sum(i) for i in zip(x, K)])

HOURLY = False
HOURLY = True
def main():
    quicktest_fld = nm.array([[1, 2, 3, 4, 5, 6, 7, 9, 0, 3, 5, 3, 6, 8],
              [1, 1, 3, 4, 5, 6, 7, 9, 0, 4, 5, 3, 6, 8],
              [8, 2, 3, 4, 5, 6, 7, 9, 0, 6, 5, 3, 6, 15],
              [7, 3, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [6, 4, 3, 4, 5, 6, 7, 9, 0, 9, 8, 3, 6, 11],
              [1, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [2, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 1, 4, 8],
              [3, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [3, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [3, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [3, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [3, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [3, 2, 3, 4, 5, 6, 7, 9, 0, 9, 5, 3, 6, 8],
              [1, 2, 3, 4, 5, 6, 7, 9, 0, 22, 5, 3, 6, 8]])

    quicktest = False
    hours_per_period = 24

    # Assume a lag-1 dependence structure for the state space.
    lags = np.array([1, 2])  # lags to apply. Indices of embedded so these select lag of 1 and 0.
    weights = nm.array([1, 0])   # Zero weight ignores second lag in neighbor distance calculation.
    max_embd = 2  # Max amount of lagged embedding.  (I think this must be max(lags).)

    # n_lags aka m, the dimension of the state space D_i_t_j where i
    # is site index and j indexes the lags.
    n_lags = len(lags)  

    if not quicktest:
        if HOURLY:
            hours_per_period = 1
            periods_per_day = 24
            WP = np.load(HISTORICAL_HOURLY_WIND_FILE).astype(float_dtype)
            ssrd = np.load(HISTORICAL_HOURLY_SOLAR_FILE).astype(float_dtype)

        else:
            # Simulate with daily data.
            hours_per_period = 24
            periods_per_day = 1
            # Load the Downward Surface Solar Radiation
            ssrd = nm.genfromtxt("data/Solar_CF_Daily.txt",  
                                 delimiter=" ", 
                                 skip_header=1)
            ssrd = ssrd[:,1:]

            # Load the Wind Power Data
            WP = nm.genfromtxt("data/Wind_CF_Daily.txt",
                               delimiter=" ",
                               skip_header=1)
            WP = WP[:,1:]
        Fld = np.concatenate((WP, ssrd), axis=1, dtype=float_dtype)
        assert Fld.shape[1] == 1150  # 575 solar sites and 575 wind sites in NM grid
        print("%d sites in simulation" % (Fld.shape[1] / 2))
        Fld = drop_leap_days(Fld, n_lags, periods_per_day, "concatenated WP and ssrd")

    log_memory("After load from disk")
    simi = 1  # index number of first simulation
    # recommended number of neighbors is int(sqrt((day_mv * 2 + 1) * Fld.shape(0) / 365)) so use 60
    nneib = 60
    start_year = 1959

    # Account for seasonailty by restricting NN search to a +/- 30 day
    # window.  The actual window is 61 days.
    day_mv = 30  # window
    if quicktest:
        # quick test
        Fld = quicktest_fld
    print("Window (day_mv): %d" % day_mv)
    print("Recommended K is %d" % int(math.sqrt((day_mv * 2 + 1) * Fld.shape[0] / 365)))

    print(Fld.shape)
    # Solar and wind are concatenated so their are ngrid/2 sites.
    ngrid = Fld.shape[1]  
    n_times = Fld.shape[0]

    if True:
        # parallel bulk execution
        # Usage: ksts.py [first-simulation-index] [number-simulations]
        if True:
            nworkers = 1
            if len(sys.argv) > 2:
                nworkers = int(sys.argv[2])
            slurm_threads = os.environ.get("SLURM_CPUS_PER_TASK")
            if slurm_threads:
                nworkers = int((int(slurm_threads)) / 2)
            if len(sys.argv) > 2:
                nworkers = int(sys.argv[2])
            print("%d workers" % nworkers)
            sys.stdout.flush()
                    
            start = 10
            if len(sys.argv) > 1:
                start = int(sys.argv[1])
            slurm_start = os.environ.get("SLURM_ARRAY_TASK_ID")
            if slurm_start:
                start = int(slurm_start)
                # add task offset
                task_offset = int(os.getenv("SLURM_PROCID", "0"))
                start += task_offset
            print("%d simulation(s) starting with %d" % (nworkers, start))
            if nm == np:
                # makes not difference in speed
                ex_cstr = futures.ProcessPoolExecutor
                unit = "process"
            else:
                ex_cstr = futures.ThreadPoolExecutor
                unit = "thread"
            with ex_cstr(max_workers=nworkers) as executor:
                if nworkers > 1:
                    futs = []
                    for sim_i in range(start, start+nworkers):
                        # this does not work with cupy
                        print("scheduling %s simulation %d" % (unit, sim_i))
                        sys.stdout.flush()
                        
                        futs += [executor.submit(kwrap,
                                                 Fld, ngrid, n_times,
                                                 sim_i,
                                                 nneib, weights, # KNN Parameter
                                                 start_year, day_mv,  # Date Seasonality Parameters
                                                 max_embd, lags,
                                                 n_lags, hours_per_period=hours_per_period)]
                    futures.wait(futs)
                            
                else:
                    for sim_i in range(start, start+nworkers):
                        print("Simulation number %d" % sim_i)
                        result = ksts(Fld, ngrid, n_times,
                                      sim_i,
                                      nneib, weights, # KNN Parameter
                                      start_year, day_mv,  # Date Seasonality Parameters
                                      max_embd, lags,
                                      n_lags, hours_per_period=hours_per_period)
                ddir = '/fs1/scratch/home/'
                date_str = datetime.now().strftime("%Y%m%d%H%M")
                suffix = str(sim_i) + '_' + str(nneib) + '_' + date_str + '.npz'

def my_argsort(A):
    # Same algorithm used by R during validation.
    A_i = nm.array(range(len(A)))
    for j in range(1, len(A)):
        key = A[j]
        key_i = A_i[j]
        i = j - 1
        while i >= 0 and A[i] > key:
            A[i + 1] = A[i]
            A_i[i + 1] = A_i[i]
            i -= 1
        A[i + 1] = key
        A_i[i + 1] = key_i
    return A_i

if __name__ == '__main__':
    main()
