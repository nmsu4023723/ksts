This repository holds code used for a paper in the NAPS 2024 conference.

There are two source files: ksts.py and netcdf2hourly.py.  The
netcdf2hourly.py program loads netCDF data from the ERA data set and
produces intermediary numpy files which are read by the KSTS
simulator, ksts.py.

Below are the shell commands to convert ERA5 netCDF weather variable
files to power units in preparation for running KSTS simulations.  The
example loads only two years of netCDF data.  In the 2024 paper with
used 63 years. It produces data/UNFILTERED_hourly_WP.npy and
data/UNFILTERED_hourly_SP.npy, which are the input files for KSTS
simulation runs.

First, cd to the repository root directory.  Then run the following shell commands:

virtualenv venv
source venv/bin/activate
pip install -r requirements.txt 
python netcdf2hourly.py ~/Downloads/era5/era_2001.nc

# to run simulations:
python ksts.py [first-simulation-index] [number-of-simulations]
